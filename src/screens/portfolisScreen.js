import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import {Grid} from '@material-ui/core'
import Title from '../components/title'
import {getTranslate} from '../localization/index'
import Portolio from '../components/portolio'
import ReImage from '../assets/images/react.jpg'

const useStyles = makeStyles(theme=>({
    root:{
        minHeight:"100vh",
        width:'100%',
        padding:30,
        paddingTop:60,
        [theme.breakpoints.down('xs')]:{
            paddingRight:10,
            paddingLeft:10,
        }
    }
}))
export default function PortfolisScreen() {
    const classes = useStyles()
    const translate=getTranslate()
 
    return (
        <div>
            <div  className={classes.root}>
                <Title title={translate.portfolis} />
                <Grid item container xs={12} direction="row" justify='flex-start' alignItems='center'>
                    {translate.potofoliosList.map((portofolio)=>
                         <Portolio  key={portofolio.id}
                            title={portofolio.title} 
                            desc={portofolio.desc}
                            href="https://google.com"   
                            image={ReImage} />   
                    )}
                   
                   
                </Grid>
            </div>
        </div>
    )
}
