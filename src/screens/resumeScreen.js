import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Typography } from '@material-ui/core'
import Title from '../components/title'
import { getTranslate } from '../localization'
import Skill from '../components/skill'
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter'
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary'
import MyStepper from '../components/MyStepper'




const useStyles = makeStyles(theme => ({
    root: {
        minHeight: "100vh",

    },
    skillGrid: {
        paddingTop: 60,
        paddingBottom: 60,
        paddingRight: 30,
        paddingLeft: 30,
        [theme.breakpoints.down('xs')]:{
            paddingRight: 5,
            paddingLeft: 5, 
        }

    },
    slkillSecondTitle:{
        marginLeft:10
    }
   


}))

export default function ResumeScreen() {
    const classes = useStyles()
    const translate = getTranslate()
    
    return (
        <Grid container justify="flex-start" alignItems="flex-start" className={classes.root}>
            <Grid className={classes.skillGrid} item container xs={12}>

                <Title title= {translate.myskill} />
                <Grid container direction="row">
                    <Grid item xs={12} md={6}  >
                        <Skill value={95} title={translate.html} /> 
                        <Skill value={90} title={translate.css} /> 
                    </Grid>
                    <Grid item xs={12} md={6}  >
                        <Skill  value={80} title={translate.javascript}/> 
                        <Skill  value={75} title={translate.reactjs}/> 
                    </Grid>
                </Grid>
            </Grid>

            <Grid className={classes.skillGrid} item container xs={12}>

                <Title title={translate.resume} />
                <Grid container direction="row" justify='flex-start' alignItems='center'>
                    <BusinessCenterIcon style={{fontSize:40}} />
                    <Typography className={classes.slkillSecondTitle} variant="h4">
                        {translate.workingexperience}
                    </Typography>
                </Grid>
                <Grid container style={{marginTop:15,marginBottom:15}} >
                    <MyStepper steps={translate.workingSteps} />
                </Grid>
                <Grid container direction="row" justify='flex-start' alignItems='center'>
                    <LocalLibraryIcon style={{fontSize:40}} />
                    <Typography className={classes.slkillSecondTitle} variant="h4">
                    {translate.educationalqualifications}  
                    </Typography>
                </Grid>
                <Grid container style={{marginTop:15,marginBottom:15}} >
                    <MyStepper steps={translate.educationalSteps} />
                </Grid>
            </Grid>

        </Grid>
    )
}
