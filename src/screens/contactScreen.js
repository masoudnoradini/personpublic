import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Typography, TextField, Button } from '@material-ui/core'
import Title from '../components/title'
import { getTranslate } from '../localization'
import './contact.css'
import Contact from '../components/contact'
import CallIcon from '@material-ui/icons/CallOutlined';
import EmailIcon from '@material-ui/icons/EmailOutlined';
import RoomIcon from '@material-ui/icons/RoomOutlined';



const useStyles = makeStyles(theme => ({
    root: {
        minHeight: "100vh",

    },

    contactGrid: {
        paddingTop: 60,
        paddingBottom: 60,
        paddingRight: 30,
        paddingLeft: 30,
        [theme.breakpoints.down('xs')]: {
            paddingRight: 5,
            paddingLeft: 5,
        }

    },
    FormGrid: {
        padding: 10,

    },
    detailsGrid: {
        padding: 10
    },
    getintouch: {
        textAlign: 'left'
    },
    iconStyle:{
      fontSize:30,
      
      

    }




}))

export default function Contactscreen() {
    const classes = useStyles()
    const translate = getTranslate()
    return (
        <Grid container justify="flex-start" alignItems="flex-start" className={classes.root}>
            <Grid className={classes.contactGrid} item container xs={12}>

                <Title title={translate.contactme} />
                <Grid container direction="row">
                    <Grid item xs={12} md={6} className={classes.FormGrid} >
                        <Typography className={classes.getintouch} variant='h4'> {translate.getintouch} </Typography>
                        <TextField
                            style={{ marginTop: 25 }}
                            size="medium"
                            fullWidth
                            required
                            label={translate.enteryourname}
                            variant="outlined"

                        />
                        <TextField
                            style={{ marginTop: 25 }}
                            size="medium"
                            fullWidth
                            required
                            label={translate.enteryouremail}
                            variant="outlined"

                        />
                        <TextField
                            style={{ marginTop: 25 }}
                            size="medium"
                            fullWidth
                            required
                            label={translate.enteryoursubject}
                            variant="outlined"

                        />
                        <TextField
                            style={{ marginTop: 25 }}
                            size="medium"
                            fullWidth
                            required
                            label={translate.enteryourmessage}
                            variant="outlined"

                            multiline={true}
                            rows={5}
                        />
                        <Button variant="contained" color="primary" size="large"
                            style={{ marginTop: 25, display: 'flex' }}>
                            {translate.sendmessaeg}
                        </Button>
                    </Grid>
                    <Grid item xs={12} md={6} className={classes.detailsGrid} >
                        <Contact
                            icon={<CallIcon  className={classes.iconStyle}/>}
                            title={translate.phone} 
                            text1="989387005113+"  
                            text2="989387005113+"
                        />
                        <Contact
                            icon={<EmailIcon  className={classes.iconStyle}/>}
                            title={translate.email}  
                            text1="masoud.noradini@gmail.com"  
                            text2="admin.sitename@example.com"
                        />
                        <Contact
                            icon={<RoomIcon  className={classes.iconStyle}/>}
                            title={translate.address} 
                            text1={translate.addressdetail}
                            text2="989387005113+"
                        />
                      
                    </Grid>
                </Grid>
            </Grid>


        </Grid>
    )
}
