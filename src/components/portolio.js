import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Typography, Modal } from '@material-ui/core'



const useStyles = makeStyles(theme => ({
    root: {
        padding: 15,
        display: 'flex',
        flexDirection: 'column',
        marginBottom: 15
    },
    image: {
        width: '100%',
        cursor: 'pointer',
        "&:hover": {
            transform: 'rotate(360deg)',
            transition: '1s'

        }
 
    },
    title: {
        textAlign: 'left',
        transition: '.4s',
        cursor: 'pointer',
        "& a": {
            textDecoration: 'none',
            color: "#fff",
            "&:hover": {
                color: theme.palette.primary.main,
                transition: '.4s'
            }
        }
    },
    Modalimage: {
        width: "100%",
        height: "100%",
     },
     modal:{
         width:"60%",
         height:"80%",
         marginLeft:'auto',
         marginRight:'auto',
         marginTop:60,
         [theme.breakpoints.down('xs')]:{
            width:"90%",
            height:"50%", 
         }
     }
}))
export default function Portolio({ image, title, desc, href }) {
    const classes = useStyles()
    const [open, setOpen] = useState(false)
    return (
        <>
            <Grid item lg={4} className={classes.root}>
                <img
                onClick={() => setOpen(true)} 
                className={classes.image} src={image} alt={title}/>
            <Typography className={classes.title} variant="h5"><a href={href}> {title} </a></Typography>
            <Typography variant='body1'> {desc} </Typography>
        </Grid>
        <Modal className={classes.modal} open={open} onClose={()=>setOpen(false)}>
            <img className={classes.Modalimage} src={image} alt={title} />

        </Modal>
        </>
    )
}
