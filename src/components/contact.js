import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Typography } from '@material-ui/core'


const useStyles = makeStyles(theme => ({
    root: {
        width: "100%",
        marginTop: 20
    },
    main: {
        cursor: 'pointer',
        backgroundColor: "#191d2b",
        zIndex: 1,
        border: "1px solid #2e344e",
        borderTop: "5px solid #2e344e ",
        padding: theme.spacing(8),
        transition: '.4s',
        '&:hover': {
            borderTopColor: theme.palette.primary.main
        }

    },

    title: {
       textAlign:'left'
    },
    desc: {
       cursor:'pointer',
       transition:'.5s',
       '&:hover':{
           color:theme.palette.primary.main,
           transition:'.5s',
       }
    },
    divIcon:{
        marginRight:25,
        margin:10,
        padding:10,
        paddingBottom:5,
        border:"1px solid #2e344e"
    },
    divDetails:{
        flex:1,display:'flex',flexDirection:'column'
    }
}))

export default function Contact({ title,text1,text2, icon }) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <Grid container direction='row' justify='flex-start' alignItems='center' className={classes.main}>
                <div className={classes.divIcon}>
                    {icon}
                </div>

                <div className={classes.divDetails}>
                    <Typography className={classes.title} variant="h6"> {title} </Typography>
                    <Typography className={classes.desc} variant="body1">
                         {text1} 
                    </Typography>
                    <Typography className={classes.desc} variant="body1">
                         {text2} 
                    </Typography>
                </div>
            </Grid>
        </div>
    )
}
