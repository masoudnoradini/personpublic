import React from 'react'
import { Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'


const useStyles = makeStyles(theme => ({
    root: {
        position: 'relative',
        marginBottom: 60,
        height: 75,
        width: '100%'
    },
    title: {
        textAlign: 'left',
        "&::after": {
            content: "''",
            position: 'absolute',
            left: 0,
            top: 'auto',
            bottom: 0,
            width: 35,
            height: 5,
            borderRadius: 100,
            background: '#037fff',
            zIndex:1
            
        },
        "&::before": {
            content: "''",
            position: 'absolute',
            left: 0,
            top: 'auto',
            bottom: 0,
            width: 100,
            height: 5,
            borderRadius: 100,
            background: "rgba(3, 127, 255, 0.3)",
            zIndex:1
        }

    },
    subtitle: {
        fontSize: '3.2rem',
        position: 'absolute',
        left: 0,
        top: '100%',
        lineHeight: "1rem",
        fontWeight: 900,
        color: 'rgba(25,29,43,.44)',
        display: 'inline-block',
        zIndex:0,
        transform: 'translateY(-40%)'
    }
}))
export default function Title({title}) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <Typography className={classes.title} variant="h2" >{title}</Typography>
            <span className={classes.subtitle}>{title}</span>

        </div>
    )
}
